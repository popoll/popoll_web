#!/bin/python
try:
    import fire
except ModuleNotFoundError:
    import pip
    pip.main(['install', 'fire'])
    import fire
import os

class Lifecycle(object):
    
    def _run(self, cmd):
        print(f'>>>>> {cmd}')
        os.system(cmd)
        
    def _run_python(self, cmd):
        return self._run(f'python -m {cmd}')

    def install(self):
        self._run('npm install')

    def build(self):
        self._run('npm build')

    def run(self, open=False):
        self._run(f'npm run dev -- --host {"--open" if open else ""}')

if __name__ == '__main__':
    fire.Fire(Lifecycle)
