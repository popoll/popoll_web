import { writable, type Writable } from 'svelte/store'
import { browser } from '$app/environment';
import { uuid } from '../utils';
import type { Poll } from '$lib/model/poll';

const KEY: string = 'popoll.villard.me.sessionid';

export const sessionId: Writable<string> = writable(browser && localStorage.getItem(KEY) || uuid())
sessionId.subscribe((val) => {
  if (browser) return (localStorage.setItem(KEY, val));
})

export const poll: Writable<string> = writable('');
export const pollColor: Writable<string> = writable('#5c636a');
export const pollName: Writable<string> = writable('');

export const isDebug: Writable<boolean> = writable(false);
export const polls: Writable<Poll[]> = writable([]);