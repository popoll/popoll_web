import { Answer } from "$lib/model/answer";
import { DatePayload } from "$lib/model/date_payload";
import { DDate } from "$lib/model/ddate";
import { Instrument } from "$lib/model/instrument";
import { Session } from "$lib/model/session";
import { UserOut } from "$lib/model/user_out";
import { Option } from "$lib/model/option";
import { UNKNOWN_ERROR, POLL_NOT_EXIST, POLL_ALREADY_EXISTS, USER_ALREADY_EXISTS, NETWORK_ISSUE } from '$lib/model/errors';
import { UserDetails } from "$lib/model/user_details";
import { User } from "$lib/model/user";
import { Poll } from "$lib/model/poll";
import { PUBLIC_BACKEND_URL } from "$env/static/public";



export async function createPoll(poll: string, name: string, instruments: string[], color: string) {
    const rs = await fetch(`${PUBLIC_BACKEND_URL}/${poll}`, {
        method: 'POST',
        body: JSON.stringify({
            name: name,
            instruments: instruments,
            color: color
        }),
        headers: {
            'Content-Type': 'application/json'
        }
    });
    if (rs.ok) {
        const json = await rs.json();
        return json['id'];
    } else if (rs.status == 409) {
        throw POLL_ALREADY_EXISTS;
    }
}




export async function getInstruments(poll: string, used_only: boolean): Promise<Instrument[]> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/instrument?used_only=${used_only}`);
    const json = await response.json();
    return json['instruments'].map((data: any) => new Instrument(data));
}







export async function getUsers(poll: string): Promise<User[]> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/user`);
    const json = await response.json();
    return json['users'].map((data: any) => new User(data));
}

export async function getUserWithInstruments(poll: string, userId: number | undefined): Promise<UserOut | undefined> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/user/${userId}?details=false`);
    const _json = await response.json();
    return new UserOut(_json);
}

export async function getUserDetails(poll: string, userId: number | undefined): Promise<UserDetails | undefined> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/user/${userId}?details=true`);
    const _json = await response.json();
    return new UserDetails(_json);
}

export async function createUser(sessionId: string, poll: string, user: UserOut): Promise<UserOut> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/user`, {
        method: 'POST',
        body: JSON.stringify(user),
        headers: {
            'Content-Type': 'application/json',
            'SessionId': sessionId
        }
    });
    if (response.ok || response.status == 404) {
        const _json = await response.json();
        return new UserOut(_json);
    } else if (response.status == 409) {
        throw USER_ALREADY_EXISTS;
    } else {
        throw UNKNOWN_ERROR;
    }
}

export async function updateUser(sessionId: string, poll: string, userId: number | undefined, user: UserOut): Promise<UserOut> {
    if (userId != undefined) {
        const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/user/${userId}`, {
            method: 'PUT',
            body: JSON.stringify(user),
            headers: {
                'Content-Type': 'application/json',
                'SessionId': sessionId
            }
        });
        if (response.ok || response.status == 404) {
            const _json = await response.json();
            return new UserOut(_json);
        } else if (response.status == 409) {
            throw USER_ALREADY_EXISTS;
        } else {
            throw UNKNOWN_ERROR;
        }
    }
    console.error('updateUser with undefined userId, we should never have this case.');
    return Promise.resolve(new UserOut({}));
}

export async function deleteUser(sessionId: string, poll: string, userId: number) {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/user/${userId}`, {
        method: 'DELETE',
        headers: {
            'SessionId': sessionId
        }
    });
}






export async function createDate(sessionId: string, poll: string, date: DDate): Promise<DDate> {
    const response = fetch(`${PUBLIC_BACKEND_URL}/${poll}/date`, {
        method: 'POST',
        body: JSON.stringify(date),
        headers: {
            'Content-Type': 'application/json',
            'SessionId': sessionId
        }
    });
    const _json = await (await response).json();
    return new DDate(_json);
}

export async function updateDate(sessionId: string, poll: string, dateId: number | undefined, date: DDate): Promise<DDate> {
    if (dateId != undefined) {
        const response = fetch(`${PUBLIC_BACKEND_URL}/${poll}/date/${dateId}`, {
            method: 'PUT',
            body: JSON.stringify(date),
            headers: {
                'Content-Type': 'application/json',
                'SessionId': sessionId
            }
        });
        const _json = await (await response).json();
        return new DDate(_json);
    }
    console.error('updateDate with undefined dateId, we should never have this case.');
    return Promise.resolve(new DDate({}));
}

export async function getDates(poll: string): Promise<DDate[]> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/date`);
    const json = await response.json();
    return json['dates'].map((data: any) => new DDate(data));
}

export async function getDDate(poll: string, dateId: number): Promise<DDate> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/date/${dateId}?details=false`);
    const json = await response.json();
    return new DDate(json);
}

export async function getDateDetails(poll: string, dateId: number): Promise<DatePayload> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/date/${dateId}?details=true`);
    const json = await response.json();
    return new DatePayload(json);
}

export async function deleteDate(sessionId: string, poll: string, dateId: number) {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/date/${dateId}`, {
        method: 'DELETE',
        headers: {
            'SessionId': sessionId
        }
    });
}




export async function getAnswer(poll: string, userId: number, dateId: number): Promise<Answer> {
    const response: Response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/answer/${userId}/${dateId}`);
    const _json = await response.json();
    return new Answer(_json);
}

export async function createAnswer(sessionId: string, poll: string, userId: number, dateId: number): Promise<Answer> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/answer`, {
        method: 'POST',
        body: JSON.stringify({
            user_id: userId,
            date_id: dateId
        }),
        headers: {
            'Content-Type': 'application/json',
            'SessionId': sessionId
        }
    });
    const _json = await response.json();
    return new Answer(_json);
}

export async function updateAnswer(sessionId: string, poll: string, answerId: number, _response: boolean | undefined): Promise<Answer> {
    const rs = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/answer/${answerId}`, {
        method: 'PUT',
        body: JSON.stringify({
            response: _response
        }),
        headers: {
            'Content-Type': 'application/json',
            'SessionId': sessionId
        }
    });
    const json = await rs.json();
    return new Answer(json);
}

export async function deleteAnswer(sessionId: string, poll: string, answerId: number) {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/answer/${answerId}`, {
        method: 'DELETE',
        headers: {
            'SessionId': sessionId
        }
    });
}




export async function getSession(poll: string, sessionId: string): Promise<Session> {
    let response;
    try {
        response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/session/${sessionId}`);
    } catch (error: unknown) {
        if (error instanceof TypeError) {
            if (error.message == "NetworkError when attempting to fetch resource.") {
                throw NETWORK_ISSUE;
            }
        }
    }
    if (response != undefined) {
        if (response.ok || response.status == 404) {
            const _json = await response.json();
            return new Session(_json);
        } else if (response.status == 400) {
            throw POLL_NOT_EXIST;
        }
    }
    throw UNKNOWN_ERROR;
}

export async function setSession(poll: string, sessionId: string, userId: number): Promise<Session> {
    const response = await fetch(`${PUBLIC_BACKEND_URL}/${poll}/session/${sessionId}`, { 
        method: 'POST',
        body: JSON.stringify({
            user_id: userId
        }),
        headers: {
            'Content-Type': 'application/json',
            'SessionId': sessionId
        }
    });
    const _json = await response.json();
    return new Session(_json);
}








export async function getOptions(poll: string): Promise<Option> {
    const rs = await fetch(`${PUBLIC_BACKEND_URL}/${poll}`);
    const _json = await rs.json();
    return new Option(_json);
}

export async function updateOptions(sessionId: string, poll: string, name: string, color: string): Promise<Option> {
    const rs = await fetch(`${PUBLIC_BACKEND_URL}/${poll}`, {
        method: 'PUT',
        body: JSON.stringify({
            name: name,
            color: color
        }),
        headers: {
            'Content-Type': 'application/json',
            'SessionId': sessionId
        }
    });
    const json = await rs.json();
    return new Option(json);
}




export async function getPolls(sessionId: string): Promise<Poll[]> {
    const rs = await fetch(`${PUBLIC_BACKEND_URL}/session/${sessionId}`);
    const _json = await rs.json();
    return _json['polls'].map((data: any) => new Poll(data));
}