import {v4 as uuidv4} from 'uuid' ;

export function uuid(): string {
    return uuidv4();
}


export function unique_id(): string {
    return Date.now().toString(36) + Math.random().toString(36).substr(2);
}