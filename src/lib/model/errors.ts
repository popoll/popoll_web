export class PError extends Error {

    title: string;
    descriptions: string[];
    rootLink: boolean;
    
    constructor(title: string, descriptions: string[], rootLink: boolean) {
        super();
        this.title = title;
        this.descriptions = descriptions;
        this.rootLink = rootLink;
    }
}

export const UNKNOWN_ERROR = new PError("Erreur inconnue", ["Il n'est pas possible de savoir facilement la cause du problème.", "Contactez Vivi ou contact@popoll.villard.me"], false);

export const NO_USER_SELECTED = new PError("Pas d'utilisateur choisi", [], false);

export const POLL_NOT_EXIST = new PError("Ce Poll n'existe pas", ["Vérifiez qu'il n'y ait pas d'erreur dans l'adresse", "Si vous voulez créer un nouveau Poll, vous pouvez le faire depuis la page principale"], true);

export const POLL_ALREADY_EXISTS = new PError("Ce Poll existe déjà", [], true);

export const USER_ALREADY_EXISTS = new PError("Cet utilisateur existe déjà", [], false);

export const REQUIRED_FIELD = new PError("Champ obligatoire", [], false);

export const NETWORK_ISSUE = new PError("Problème de connexion", ["Merci de contacter contact@popoll.villard.me si cela perdure."], false);