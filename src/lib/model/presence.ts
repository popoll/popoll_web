import check from '$lib/images/answers/check.png';
import question from '$lib/images/answers/question.png';
import x from '$lib/images/answers/x.png';

export class Presence {

    value: boolean | undefined;
    label: string;
    image: string;

    constructor(value: boolean | undefined, label: string, image: string) {
        this.value = value;
        this.label = label;
        this.image = image;
    }
}

export const PRESENT: Presence = new Presence(true, "Présent", check);
export const ABSENT: Presence = new Presence(false, "Absent", x);
export const UNKNOWN: Presence = new Presence(undefined, "Inconnu", question);

export const PRESENCES: Presence[] = [PRESENT, UNKNOWN, ABSENT];
