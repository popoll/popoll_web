import { Answer } from "./answer";
import { Instrument } from "./instrument";
import type { User } from "./user";
import { DDate } from "./ddate";
import { UserOut } from "./user_out";

export class _UserDatePayload {

    date: DDate;
    answer: Answer | undefined;

    constructor(data: any) {
        this.date = new DDate(data.date);
        this.answer = data.answer != undefined ? new Answer(data.answer) : undefined;
    }

    hasPresence(value: boolean | undefined): boolean {
        return this.answer?.response == value;
    }
}

export class UserPayload {

    user: UserOut;
    dates: _UserDatePayload[];

    constructor(data: any) {
        this.user = new UserOut(data.user);
        this.dates = data.dates.map((_data: any) => new _UserDatePayload(_data));
    }

    name(): string {
        return this.user.user.name;
    }

    id(): number {
        return this.user.user.id;
    }

    getDate(dateId: number) {
        return this.dates.filter((date) => date.date.id == dateId)[0];
    }
}