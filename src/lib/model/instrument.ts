export class Instrument {

    id: number;
    name: string;
    rank: number;

    constructor(data: any) {
        this.id = data.id;
        this.name = data.name;
        this.rank = data.rank;
    }
}