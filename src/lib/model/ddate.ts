const formatter = new Intl.DateTimeFormat('fr-FR', {
    dateStyle: 'full',
    timeStyle: 'long',
    timeZone: 'Europe/Paris'
});

export class DDate {

    id: number;
    title: string;
    date: string;
    time: string | undefined;
    end_time: string | undefined;
    is_frozen: boolean;
    is_old: boolean;

    constructor(data: any) {
        this.id = data.id;
        this.title = data.title;
        this.date = data.date;
        this.time = data.time;
        this.end_time = data.end_time
        this.is_frozen = data.is_frozen
        this.is_old = data.is_old
    }

    _date(date: string): string {
        return formatter.formatToParts(Date.parse(date)).map((p) => p.value.trim()).filter((p) => p.length > 0).slice(0, 4).join(' ');
    }

    _time(time: string): string {
        return time.slice(0, 5).toString();
    }

    datetime(): string {
        if (this.time == undefined) {
            return this._date(this.date);
        } else {
            if (this.end_time == undefined) {
                return `${this._date(this.date)} (${this._time(this.time)})`;
            } else {
                return `${this._date(this.date)} (${this._time(this.time)} - ${this._time(this.end_time)})`;
            }
        }
    }

    padded_title(): string {
        if (this.title.length > 80) {
            return this.title.slice(0, 78) + '...';
        } else {
            return this.title;
        }
    }

    str(): string {
        return `${this.datetime()} - ${this.title}`;
    }

    padded_str(): string {
        return `${this.datetime()} - ${this.padded_title()}`;
    }
}