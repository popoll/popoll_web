export class Option {

    name: string;
    color: string;

    constructor(data: any) {
        this.name = data.name;
        this.color = data.color;
    }
}