export class Session {

    id: number;
    session_id: string;
    user_id: number;

    constructor(data: any) {
        this.id = data.id;
        this.session_id = data.session_id;
        this.user_id = data.user_id;
    }
}