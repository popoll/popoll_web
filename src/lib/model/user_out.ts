import { Instrument } from "./instrument";
import { User } from "./user";

export class UserOut {

    user: User;
    main_instrument: Instrument | undefined;
    instruments: Instrument[];

    constructor(data: any) {
        this.user = new User(data.user);
        this.main_instrument = data.main_instrument != undefined ? new Instrument(data.main_instrument) : undefined;
        this.instruments = data.instruments.map((row: any) => new Instrument(row));
    }

    id(): number {
        return this.user.id;
    }

    name(): string {
        return this.user.name;
    }
}