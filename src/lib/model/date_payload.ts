import { User } from "./user";
import { DDate } from "./ddate";
import { Answer } from "./answer";
import { Instrument } from "./instrument";

export class _DateAnswerPayload {

    answer: Answer | undefined;
    user: User;
    instrument: Instrument;
    is_main_instrument: boolean;

    constructor(data: any) {
        this.answer = data.answer != undefined ? new Answer(data.answer) : undefined;
        this.user = new User(data.user);
        this.instrument = new Instrument(data.instrument);
        this.is_main_instrument = data.is_main_instrument;
    }
}

export class DatePayload {

    date: DDate;
    answers: _DateAnswerPayload[];

    constructor(data: any) {
        this.date = new DDate(data.date);
        this.answers = data.answers.map((_data: any) => new _DateAnswerPayload(_data));;
    }

    getUsersByPresenceAndInstrument(presence: boolean | undefined, instrument: Instrument): User[] {
        return this.answers.filter((a) => a.answer?.response == presence && a.instrument.id == instrument.id && a.is_main_instrument).map((a) => a.user);
    }

    getUsersByPresenceAndInstruments(presence: boolean | undefined, instruments: Instrument[]): User[] {
        const instruments_ids = instruments.map((i) => i.id);
        return this.answers.filter((a) => a.answer?.response == presence && instruments_ids.indexOf(a.instrument.id) > -1 && a.is_main_instrument).map((a) => a.user);
    }

    getUsersByInstrument(instrument: Instrument): User[] {
        return this.answers.filter((a) => a.instrument.id == instrument.id && a.is_main_instrument).map((a) => a.user);
    }

    getUsersByPresence(presence: boolean | undefined): User[] {
        return this.answers.filter((a) => a.answer?.response == presence && a.is_main_instrument).map((a) => a.user);
    }

    getTitleCount(presence: boolean | undefined, instruments: Instrument[], all_instruments: Instrument[]) {
        if (instruments.length == 0 || instruments.length == all_instruments.length) {
            return `(${this.getUsersByPresence(presence).length})`;    
        } else {
            return `(${this.getUsersByPresenceAndInstruments(presence, instruments).length})`;
        }
    }

    getUsersSecondInstrument(presence: boolean | undefined, instrument: Instrument): User[] {
        return this.answers.filter((a) => a.answer?.response == presence && a.instrument.id == instrument.id && !a.is_main_instrument).map((a) => a.user);
    }
}