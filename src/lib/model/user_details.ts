import { Answer } from "./answer";
import { Instrument } from "./instrument";
import { User } from "./user";
import { DDate } from "./ddate";
import { UserOut } from "./user_out";

export class UserDetailsDate {

    date: DDate;
    answer: Answer | undefined;

    constructor(data: any) {
        this.date = new DDate(data.date);
        this.answer = data.answer != undefined ? new Answer(data.answer) : undefined;
    }

    hasPresence(value: boolean | undefined): boolean {
        return this.answer?.response == value;
    }
}

export class UserDetails {

    user: User;
    dates: UserDetailsDate[];

    constructor(data: any) {
        this.user = new User(data.user);
        this.dates = data.dates.map((_data: any) => new UserDetailsDate(_data));
    }

    getDate(dateId: number) {
        return this.dates.filter((date) => date.date.id == dateId)[0];
    }
}