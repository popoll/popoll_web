export class Answer {

    id: number;
    user_id: number;
    date_id: number;
    response: boolean | undefined;

    constructor(data: any) {
        this.id = data.id;
        this.user_id = data.user_id;
        this.date_id = data.date_id;
        this.response = data.response;
    }
}