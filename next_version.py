import argparse
import requests

def get_options():
    parser = argparse.ArgumentParser()
    parser.add_argument('--index', '-i', default='https://registry.npmjs.org')
    parser.add_argument('--scope', default='')
    parser.add_argument('--url', default='{index}/{scope}/{module}/latest')
    parser.add_argument('module')
    return parser

def get_latest(args):
    content = requests.get(args.url.format(index=args.index, scope=args.scope, module=args.module))
    return content.json()['version']

def run(args):
    try:
        version = get_latest(args).split('.')
        version[-1] = str(int(version[-1]) + 1)
        print('.'.join(version))
    except:
        print('0.0.1')

if __name__ == '__main__':
    args = get_options().parse_args()
    run(args)